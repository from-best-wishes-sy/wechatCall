package cn.cvzhanshi.wechatpush.config;


import cn.cvzhanshi.wechatpush.entity.Weather;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

import java.util.Map;

/**
 * @author 代
 * @create 2022-08-23 22:58
 */
public class Pusher {

    public static void main(String[] args) {
        push();
    }
    private static String appId = "";//填写微信公众号接口测试账号的id
    private static String secret = "";//密码



    public static void push(){
        //1，配置
        WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(appId);
        wxStorage.setSecret(secret);
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);
        //2,推送消息
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser("")///发送用户的微信id
                .templateId("-lWtnSekX2gPLIhM6Q-ProomZc6ZjwZwXsbK2-8YgR4")//模板id
                .build();

        //3,如果是正式版发送模版消息，这里需要配置你的信息
        Weather weather = WeatherUtils.getWeather();
        templateMessage.addData(new WxMpTemplateData("riqi",weather.getDate() + "  "+ weather.getWeek(),"#ec9bad"));
        templateMessage.addData(new WxMpTemplateData("tianqi",weather.getText_now(),"#ee4863"));
        templateMessage.addData(new WxMpTemplateData("low",weather.getLow() + "","#cc163a"));
        templateMessage.addData(new WxMpTemplateData("temp",weather.getTemp() + "","#EE212D"));
        templateMessage.addData(new WxMpTemplateData("high",weather.getHigh()+ "","#cc163a" ));
        templateMessage.addData(new WxMpTemplateData("windclass",weather.getWind_class()+ "","#42B857" ));
        templateMessage.addData(new WxMpTemplateData("winddir",weather.getWind_dir()+ "","#B95EA3" ));
        templateMessage.addData(new WxMpTemplateData("caihongpi"," \n"+CaiHongPiUtils.getCaiHongPi()+"","#2f90b9"));
        templateMessage.addData(new WxMpTemplateData("lianai",JiNianRiUtils.getLianAi()+"","#FF1493"));
        templateMessage.addData(new WxMpTemplateData("shengri1",JiNianRiUtils.getBirthday_Jo()+"","#FFA500"));
        templateMessage.addData(new WxMpTemplateData("shengri2",JiNianRiUtils.getBirthday_Hui()+"","#FFA500"));
//        templateMessage.addData(new WxMpTemplateData("en",map.get("en") +"","#C71585"));
//        templateMessage.addData(new WxMpTemplateData("zh",map.get("zh") +"","#C71585"));
        templateMessage.addData(new WxMpTemplateData("saylove",CaiHongPiUtils.getSaylove()+"","#ec9bad"));
        templateMessage.addData(new WxMpTemplateData("province",weather.getProvince()+"","#e77c8e"));
        templateMessage.addData(new WxMpTemplateData("city",weather.getCity()+"","#e77c8e"));
        templateMessage.addData(new WxMpTemplateData("cityname",weather.getCityname()+"","#de3f7c"));
        templateMessage.addData(new WxMpTemplateData("zaoan",CaiHongPiUtils.getZaoan()+"","#63bbd0"));
        String beizhu = "亲爱的宝贝早上好呀！今天也要开开心心噢！";
        String ps = "下面是今天的天气情况噢！";
        if(JiNianRiUtils.getLianAi() % 365 == 0){
            beizhu = "今天是恋爱" + (JiNianRiUtils.getLianAi() / 365) + "周年纪念日！";
        }
        if(JiNianRiUtils.getBirthday_Jo()  == 0){
            beizhu = "今天是生日，生日快乐呀！";
        }
        if(JiNianRiUtils.getBirthday_Hui()  == 0){
            beizhu = "今天是生日，生日快乐呀！";
        }
        templateMessage.addData(new WxMpTemplateData("beizhu",beizhu,"#f1939c"));
        templateMessage.addData(new WxMpTemplateData("beizhu2",ps,"#ffd111"));

        try {
            System.out.println(templateMessage.toJson());

            System.out.println(wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage));
        } catch (Exception e) {
            System.out.println("推送失败：" + e.getMessage());
            e.printStackTrace();
        }
    }
}
